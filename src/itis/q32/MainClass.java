package itis.q32;


/*
В папке resources находятся два .csv файла.
Один содержит данные о группах в университете в следующем формате: ID группы, название группы, код группы
Второй содержит данные о студентах: ФИО, дата рождения, айди группы, количество очков рейтинга

напишите код который превратит содержимое файлов в обьекты из пакета "entities", выведите в консоль всех студентов,
в читабельном виде, с информацией о группе
Используя StudentService, выведите:

1. Число групп с только совершеннолетними студентами
2. Самую маленькую группу
3. Отношение группа - сумма балов студентов фамилия которых совпадает с заданной строкой
4. Отношения студент - дельта баллов до проходного порога (порог передается параметром),
 сгруппированные по признаку пройден порог, или нет

Требования к реализации: все методы в StudentService должны быть реализованы с использованием StreamApi.
Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
*/

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

public class MainClass {
    public Map<Group, Student> studentMap = new HashMap<>();
    public List<Group> groupList = new ArrayList<>();
    public List<Student> studentList = new ArrayList<>();
    private StudentService studentService = new StudentServiceImpl();

    public static void main(String[] args) throws IOException {
        new itis.q32.MainClass().run(
                "",
                "");
    }

    private void run(String studentsPath, String groupsPath) throws IOException {
        BufferedReader studentReader = new BufferedReader(new FileReader(studentsPath));
        BufferedReader groupReader = new BufferedReader(new FileReader(groupsPath));
        String groupLine;
        while ((groupLine = groupReader.readLine()) != null) {
            groupLine = groupLine.replaceAll(",", "");
            String[] groupArr = groupLine.split(" ");
            Group group = new Group(groupArr[1], Long.parseLong(groupArr[0]), groupArr[2]);
            groupList.add(group);
        }
        groupReader.close();
        String studentLine;
        while ((studentLine = studentReader.readLine()) != null) {
            studentLine = studentLine.replaceAll(",", "");
            String[] strings = studentLine.split(" ");
            String fullname = strings[0] + " " + strings[1] + " " + strings[2];
            Student student = new Student(fullname, getGroupByID(Long.parseLong(strings[4])),
                    Integer.parseInt(strings[5]), LocalDate.parse(strings[3]));
            studentList.add(student);
        }
        studentReader.close();
        for (Student student : studentList) {
            System.out.println(student.toString());
            System.out.println("----------------------------------");
        }
    }

    public Group getGroupByID(Long id) {
        for (Group group :
                groupList) {
            if (group.getId().equals(id)) {
                return group;
            }
        }
        return null;
    }
}













