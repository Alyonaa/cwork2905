package itis.q32;

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentServiceImpl implements StudentService {


    @Override
    public Group getSmallestGroup(List<Student> students) {
        HashMap<Long, Integer> counter = new HashMap<>();
        students.stream()
                .forEach(student -> {
                    if(counter.containsKey(student.getGroup())){
                        counter.put(student.getGroup().getId(), counter.get(student.getGroup()) + 1 );
                    }
                    else{
                        counter.put(student.getGroup().getId(), 1);                    }
                });
        return new MainClass().getGroupByID(Long.parseLong(String.valueOf(counter.entrySet().stream().
                min(Comparator.comparingInt(count->count.getValue())))));
    }

    @Override
    public Integer countGroupsWithOnlyAdultStudents(List<Student> students) {
        return null;
    }

    @Override
    public Map<String, Integer> getGroupScoreSumMap(List<Student> students, String studentSurname) {
        return null;
    }

    @Override
    public Map<Boolean, Map<String, Integer>> groupStudentScoreWithThreshold(List<Student> students, Integer threshold) {
        return null;
    }
}
